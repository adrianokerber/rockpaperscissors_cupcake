# Rock Paper Scissors#

This project is a Rock Paper Scissors game developed for the Cupcake hiring process.

NOTE: all the code and tools used in the project are either mine (Adriano Kerber) or documented with author's references.

### Project languages ###

* Languages: Javascript and HTML5.

### Notes ###

* Every ___Scene___ class must have two methods: ```Start(context)``` and ```Finish()```.
* Were created three classes to handle text, image, and primitive draw over the canvas in a simpler way. They are: DrawableObject (Image), DrawableText (Text) and DrawableRectangle (Rect).
* The game has a song now :D.
* Click to start the game and then use the LeftArrow for rock, RightArrow for scissor and DownArrow for paper.