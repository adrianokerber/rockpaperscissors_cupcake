var MenuScene = function () {
    this.Start = function (context) {
        // Definindo o layout da cena

        // Imagem de t�tulo
        var image = new Image();
        image.src = PathsTable.Resources.Sprites.title;
        image.onload = function () {
            var w = 200;
            var h = 100;
            context.drawImage(image, context.canvas.width / 2 - w / 2, context.canvas.height / 2 - (h * 1.5), w, h);
        };
        
        // Adiciona o texto
        context.save();
        var text = "Click to begin!";
        var fontSize = 30;
        context.font = fontSize + "px Arial";
        context.fillText(text, context.canvas.width / 2 - (((fontSize / 2.1) * text.length) / 2), context.canvas.height * 0.5);
        context.restore();

        // Adiciona o texto de informa��o (3 � o tamanho m�dio para cada letra)
        context.save();
        var textInfo = "Developed by Adriano Kerber in 2017";
        fontSize = 15;
        context.font = fontSize + "px Arial";
        context.fillText(textInfo, context.canvas.width / 2 - (((fontSize / 2.1) * textInfo.length) / 2), context.canvas.height * 0.9);
        context.restore();

        // Input
        context.canvas.onclick = function () {
            Finish(context);
        };
    }

    function Finish(context) {
        ClearScene(context);

        // Remove o evento de clique
        context.canvas.onclick = null;

        // Chama a pr�xima cena
        var gs = new GameScene();
        gs.Start(context);
    };
};