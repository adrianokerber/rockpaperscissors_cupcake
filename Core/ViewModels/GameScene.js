var GameScene = function () {
    var FPS = 30; // Marca o FPS do jogo
    var ctx; // Contexto
    var data = new UserData();
    var state = GameState.PLAYING; // Estado do jogo
    var opponentSelection = Option.NONE;
    var textResult;
    var playerChoice, opponentChoice;

    // Vari�vel auxiliar
    var keyPressedRecently = false;

    // Objetos desenhaveis
    var objects = new Array(), objects_final = new Array();

    this.Start = function (context) {
        // Define os updates da cena
        StartInternal(context, Draw, Update);

        // Adiciona evento para tratar as teclas por callback
        document.addEventListener("keydown", keydownHandler);
    };

    function Finish(context) {
        ClearScene(context);

        // Remove os eventos para tratar as teclas por callback
        document.removeEventListener("keydown", keydownHandler);
    };

    function keydownHandler(e) {
        if (!keyPressedRecently) { // Se nenhuma tecla foi pressionada recentemente
            keyPressedRecently = true;

            // Quando teclas s�o pressionadas...
            if (state != GameState.FINAL_DISPLAY) {
                if (e.keyCode == "37" || e.keyCode == "65") // Esquerda;
                    data.selection = Option.ROCK;
                if (e.keyCode == "39" || e.keyCode == "68") // Direita;
                    data.selection = Option.SCISSOR;
                if (e.keyCode == "40" || e.keyCode == "83") // Baixo;
                    data.selection = Option.PAPER;
            } else {
                // Atualiza o estado
                state = GameState.PLAYING;
                data.selection = Option.NONE;
            }

            setTimeout(function () {
                keyPressedRecently = false;
                console.log("Press again!");
            }, 200); // Daqui a 0.2 segundos ir� liberar o input
        }
    }

    // Define as chamadas de desenho e update
    function StartInternal(context, draw, update) {
        ctx = context;

        // Cria objetos a serem desenhados
        var w = 50, h = 50;
        var option_y = ctx.canvas.height * 0.7;
        var arrow_y = ctx.canvas.height * 0.8;

        // Caixas de fundo dos bot�es/opc�es
        var obj = new DrawableRectange(ctx.canvas.width * 0.3, option_y, w, h + h + (arrow_y - (option_y + h)))
        objects.push(obj);
        var obj = new DrawableRectange(ctx.canvas.width * 0.5, option_y, w, h + h + (arrow_y - (option_y + h)))
        objects.push(obj);
        var obj = new DrawableRectange(ctx.canvas.width * 0.7, option_y, w, h + h + (arrow_y - (option_y + h)))
        objects.push(obj);

        obj = new DrawableObject(PathsTable.Resources.Sprites.rock, ctx.canvas.width * 0.3, option_y, w, h);
        obj.id = DrawableIDsTable.ROCK;
        objects.push(obj);
        obj = new DrawableObject(PathsTable.Resources.Sprites.paper, ctx.canvas.width * 0.5, option_y, w, h);
        obj.id = DrawableIDsTable.PAPER;
        objects.push(obj);
        obj = new DrawableObject(PathsTable.Resources.Sprites.scissor, ctx.canvas.width * 0.7, option_y, w, h);
        obj.id = DrawableIDsTable.SCISSOR;
        objects.push(obj);
        obj = new DrawableObject(PathsTable.Resources.Sprites.leftarrow, ctx.canvas.width * 0.3, arrow_y, w, h);
        obj.id = DrawableIDsTable.LEFT_ARROW;
        objects.push(obj);
        obj = new DrawableObject(PathsTable.Resources.Sprites.downarrow, ctx.canvas.width * 0.5, arrow_y, w, h);
        obj.id = DrawableIDsTable.DOWN_ARROW;
        objects.push(obj);
        obj = new DrawableObject(PathsTable.Resources.Sprites.rightarrow, ctx.canvas.width * 0.7, arrow_y, w, h);
        obj.id = DrawableIDsTable.RIGHT_ARROW;
        objects.push(obj);

        // Cria o texto superior
        obj = new DrawableText("Select your option with the arrow keys", ctx.canvas.width * 0.5, ctx.canvas.height * 0.3, ctx.canvas.width, ctx.canvas.height);
        obj.color = "rgb(0,0,0)";
        obj.font = "30px Arial";
        obj.align = "center";
        objects.push(obj);
        

        // FINAL_DISPLAY objects:
        obj = new DrawableText("", ctx.canvas.width * 0.5, ctx.canvas.height * 0.4, ctx.canvas.width, ctx.canvas.height);
        obj.font = "30px Arial";
        obj.color = "rgb(0, 0, 0)";
        obj.align = "center";
        objects_final.push(obj);
        textResult = obj;

        // Imagens do FINAL_DISPLAY
        // Imagem do que o jogador selecionou
        playerChoice = new DrawableObject(PathsTable.Resources.Sprites.title, ctx.canvas.width * 0.35, ctx.canvas.height * 0.5, 80, 80);
        objects_final.push(playerChoice);
        // Imagem do que o oponente selecionou
        opponentChoice = new DrawableObject(PathsTable.Resources.Sprites.title, ctx.canvas.width * 0.515, ctx.canvas.height * 0.5, 80, 80);
        objects_final.push(opponentChoice);
        // X que fica entre as imagens
        obj = new DrawableText("x", ctx.canvas.width * 0.5, ctx.canvas.height * 0.5, ctx.canvas.width, ctx.canvas.height);
        obj.font = "15px Arial";
        objects_final.push(obj);

        // Mensagem final
        obj = new DrawableText("Press ANY key to rematch...", ctx.canvas.width * 0.5, ctx.canvas.height * 0.9, ctx.canvas.width, ctx.canvas.height);
        obj.font = "15px Arial";
        obj.align = "center";
        objects_final.push(obj);


        // Define os eventos a serem chamados por callback:
        setInterval(draw, 1000 / FPS); // Define a fun��o de desenho a ser chamada por callback;
        setInterval(update, 1000 / FPS); // Define a fun��o de update a ser chamada por callback;

        // SOM
        var canal = document.createElement("audio");
        canal.src = PathsTable.Resources.SoundFX.theme;
        canal.loop = true;
        canal.play();
    };

    // Callback que faz todos os desenhos in-game
    function Draw() {
        // LIMPA O CANVAS
        ClearScene(ctx);

        // Desenha cada objeto:

        // HUD
        var HUD = new DrawableText("Score: You " + data.score.mine + " x " + data.score.thy + " AI", ctx.canvas.width / 2, ctx.canvas.height * 0.05, ctx.canvas.width, ctx.canvas.height);
        HUD.align = "center";
        HUD.font = "15px Arial";
        HUD.Draw(ctx);

        // Estado de desenho
        switch (state) {
            case GameState.PLAYING:
                // Desenha as caixas sob as op��es
                ctx.save();
                ctx.fillStyle = "rgb(125,125,125)";

                // Desenha todo o resto
                for (var i = 0; i < objects.length; i++){
                    objects[i].Draw(ctx);
                }

                ctx.restore();
                break;
            case GameState.FINAL_DISPLAY:
                // Desenha a tela final com todas as op��es

                for (var i = 0; i < objects_final.length; i++) {
                    objects_final[i].Draw(ctx);
                }

                break;
            default:
                // N�o desenha nada
                break;
        }
    };

    // Callback que faz todas as atualiza��es in-game:
    function Update() {
        // L�gica de cada estado de jogo
        switch (state) {
            case GameState.PLAYING:
                // L�gica de sele��o da op��o pelo jogador
                if (data.selection != Option.NONE) {
                    state = GameState.OPPONENTS_TURN; // Muda o estado do jogo
                }
                break;
            case GameState.OPPONENTS_TURN:
                // Executar l�gica do oponente
                // Sele��o do oponente
                opponentSelection = Math.random() * 3 + 1;
                if (opponentSelection < 2) {
                    opponentSelection = Option.ROCK;
                } else if (opponentSelection > 1 && opponentSelection < 3) {
                    opponentSelection = Option.PAPER;
                } else if (opponentSelection > 2) {
                    opponentSelection = Option.SCISSOR;
                }

                // Troca estado
                state = GameState.GAME_LOGIC;
                break;
            case GameState.GAME_LOGIC:
                // Definir ganhador
                /*
                Regras:
                    > Rock > Scissor > Paper >
                */
                result = data.selection + opponentSelection; // Monta o resultado a ser comparado
                switch (result) {
                    case Option.ROCK + Option.SCISSOR:
                    case Option.PAPER + Option.ROCK:
                    case Option.SCISSOR + Option.PAPER:
                        // WIN
                        result = "Won";
                        data.score.mine += 1;
                        break;
                    case Option.ROCK + Option.ROCK:
                    case Option.PAPER + Option.PAPER:
                    case Option.SCISSOR + Option.SCISSOR:
                        // DRAW
                        result = "Draw";
                        data.score.mine += 1;
                        data.score.thy += 1;
                        break;
                    case Option.ROCK + Option.PAPER:
                    case Option.PAPER + Option.SCISSOR:
                    case Option.SCISSOR + Option.ROCK:
                        // LOSE
                        result = "Lost";
                        data.score.thy += 1;
                        break;
                }
                textResult.value = result; // Adiciona o texto ao componente de desenho

                // Define o caminho das imagens dos componentes a serem desenhados
                var path;
                // Seleciona o caminho da imagem do jogador
                switch (data.selection) {
                    case Option.ROCK:
                        path = PathsTable.Resources.Sprites.rock;
                        break;
                    case Option.PAPER:
                        path = PathsTable.Resources.Sprites.paper;
                        break;
                    case Option.SCISSOR:
                        path = PathsTable.Resources.Sprites.scissor;
                        break;
                }
                playerChoice.UpdateSource(path);
                // Seleciona o caminho da imagem do oponente
                switch (opponentSelection) {
                    case Option.ROCK:
                        path = PathsTable.Resources.Sprites.rock;
                        break;
                    case Option.PAPER:
                        path = PathsTable.Resources.Sprites.paper;
                        break;
                    case Option.SCISSOR:
                        path = PathsTable.Resources.Sprites.scissor;
                        break;
                }
                opponentChoice.UpdateSource(path);

                // Atualiza estado de jogo
                state = GameState.FINAL_DISPLAY;

                break;
        }
    }
};