var GameState = {
    PLAYING: 0,
    OPPONENTS_TURN: 1,
    GAME_LOGIC: 2,
    FINAL_DISPLAY: 3
};