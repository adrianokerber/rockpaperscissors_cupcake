var Option = {
    NONE: "none",
    ROCK: "rock",
    PAPER: "paper",
    SCISSOR: "scissor"
};

var UserData = function () {
    this.selection = Option.NONE; // Nada selecionado
    this.score = {
        mine: 0,
        thy: 0
    };
};