var DrawableIDsTable = {
    ROCK: "rock",
    PAPER: "paper",
    SCISSOR: "scissor",
    LEFT_ARROW: "leftarrow",
    DOWN_ARROW: "downarrow",
    RIGHT_ARROW: "rightarrow"
};