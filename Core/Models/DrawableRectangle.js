var DrawableRectange = function (x = 0.1, y = 0.1, width = 0.1, height = 0.1) {
    this.x = x;
    this.y = y;
    this.width = width;
    this.height = height;
    this.enabled = true; // Habilitado para desenhar
    this.id = ""; // Nome identificador
    // Atributos de renderização
    this.color; // Recebe um fillStyle

    this.Draw = function (context) {
        if (this.enabled) {
            context.save();

            context.fillStyle = this.color;
            context.fillRect(this.x, this.y, this.width, this.height);

            context.restore();
        }
    };
};