var PathsTable = {
    Resources: {
        resourcesPath: "Resources",
        Sprites: {
            spritesPath: "Resources/Sprites",

            rock: "Resources/Sprites/Rock.png",
            paper: "Resources/Sprites/Paper.png",
            scissor: "Resources/Sprites/Scissor.png",

            title: "Resources/Sprites/Title.png",

            leftarrow: "Resources/Sprites/leftarrow.png",
            downarrow: "Resources/Sprites/downarrow.png",
            rightarrow: "Resources/Sprites/rightarrow.png"
        },
        SoundFX: {
            theme: "Resources/SoundFX/Pipe_Choir_-_04_-_Fortress.mp3" // http://freemusicarchive.org/genre/Ambient/
        }
    }
};