var DrawableObject = function (source = "path", x = 0.1, y = 0.1, width = 0.1, height = 0.1) {
    this.x = x;
    this.y = y;
    this.width = width;
    this.height = height;
    this.id = ""; // Nome identificador
    var ready = false;
    this.enabled = true; // Habilitado para desenhar
    this.source = source;
    // Define a imagem
    this.image = new Image();
    this.image.src = this.source;
    this.image.onload = function () {
        ready = true;
    };

    this.Draw = function (context) {
        if (ready && this.enabled)
            context.drawImage(this.image, this.x, this.y, this.width, this.height);
    };

    // Troca ou recarrega a imagem
    this.UpdateSource = function (newSource) {
        ready = false;
        this.source = newSource;
        this.image = new Image();
        this.image.src = this.source;
        this.image.onload = function () {
            ready = true;
        };
    };
};