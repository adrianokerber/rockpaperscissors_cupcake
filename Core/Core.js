/// Criado por Adriano Kerber
/// Classe principal que inicia o jogo

var Core = function () {
    // Define onde o jogo ser� desenhado
    this.StartOn = function (canvasName, scene) {
        // Inicia o canvas
        var canvas = document.getElementById(canvasName);
        if (canvas.getContext) { // Se o canvas foi criado corretamente
            var context = canvas.getContext("2d"); // Pega o contexto de desenho

            if (context) {
                var s = new scene();
                // Inicia o jogo
                s.Start(context);
            }
        }
    };
};

// Utilidades:

// Limpar cena
function ClearScene(context) {
    // Limpa o canvas
    context.clearRect(0, 0, context.canvas.width, context.canvas.height);
};